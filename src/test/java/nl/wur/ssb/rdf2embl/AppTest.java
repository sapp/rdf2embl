package nl.wur.ssb.rdf2embl;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

  public void testGap() throws Exception {
    String[] args = {"-input", "./src/test/resources/genomeGap.hdt", "-output",
        "./src/test/resources/TEST.embl", "-organism", "Mycoplasma", "-strain", "", "-substrain",
        "", "-keywords", "A,B,C,D,WGS", "-authors", "Jasper J. Koehorst, Bart Nijsse", "-taxon",
        "1314", "-codon", "11", "-locus", "FZH_", "-title", "MY PAPER", "-journal", "MY JOURNAL",
        "-consortium", "CONSORTIUUUM", "-projectid", "PRJNA40779 ", "-dataclass", "WGS", "-writer",
        "embl", "-scaffold", "TOF_"};
    App.main(args);
  }

  public void testOthers() throws Exception {
    @SuppressWarnings("unused")
    String[] args = {"-input", "./testing/bart.hdt", "-output", "./testing/TEST.embl", "-organism",
        "Mycoplasma", "-strain", "funny", "-substrain", "", "-keywords", "A,B,C,D", "-authors",
        "Jasper J. Koehorst, Bart Nijsse", "-taxon", "1314", "-codon", "11", "-locus", "FZH_",
        "-title", "MY PAPER", "-journal", "MY JOURNAL", "-consortium", "CONSORTIUUUM", "-dataclass",
        "WGS", "-projectid", "PRJNA40779", "-writer", "embl"};

    @SuppressWarnings("unused")
    String[] args2 = {"-input", "./testing/TEST.hdt", "-output", "./testing/TEST.embl", "-organism",
        "Mycoplasma", "-strain", "funny", "-substrain", "", "-keywords", "A,B,C,D", "-authors",
        "Jasper J. Koehorst, Bart Nijsse", "-taxon", "1314", "-codon", "11", "-locus", "FZH_",
        "-title", "MY PAPER", "-journal", "MY JOURNAL", "-consortium", "CONSORTIUUUM", "-dataclass",
        "WGS", "-projectid", "PRJNA40779", "-writer", "embl"}; // ,"-pathwaytools"
    // };

    // App.main(args1);
    // System.out.println("ENA VALIDATOR");
    // String command = "java -classpath ./resource/JAVA/embl-client.jar
    // uk.ac.ebi.client.EnaValidator -r " + "./testing/TEST.embl";
    // System.out.println(command);
    // new ExecCommand(command);
  }
}
