package nl.wur.ssb.proteinAnnotation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;

import nl.wur.ssb.HDT.HDT;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.data.Domain;
import nl.wur.ssb.RDFSimpleCon.data.Property;
import nl.wur.ssb.RDFSimpleCon.data.RDFSubject;
import nl.wur.ssb.RDFSimpleCon.data.RDFType;

public class App {
	private static CommandLine arguments;
	private static RDFSimpleCon SAPPSource;

	public static RDFSimpleCon main(String[] args) throws Exception {
		arguments = CommandParser.argsValidator(args);
		// Logger.getRootLogger().setLevel(Level.DEBUG);
		loadRDF();
		deletePreviousProteinAnnotation();
		proteinAnnotation();
		return SAPPSource;
	}

	private static void deletePreviousProteinAnnotation() throws Exception {
		// Removes previous annotation performed by previous annotation runs.
		// Does not remove original genbank annotation
		SAPPSource.runUpdateQuery("removePreviousannotation.txt");
	}

	private static void proteinAnnotation() throws Exception {
		Domain domain = new Domain(SAPPSource);
		new Property(domain, "ssb:product");
		new RDFType(domain, "ssb:Protein");

		List<Protein> proteinList = new ArrayList<Protein>();
		String proteinIRI = "";
		Protein proteinObject = null;
		List<ProteinDomain> domainList = new ArrayList<ProteinDomain>();
		for (ResultLine item : SAPPSource.runQuery("getProteinInformation.txt", true)) {
			if (!proteinIRI.equals(item.getIRI("protein"))) {
				proteinObject = new Protein();
				proteinObject.setProteinScore(-10);
				proteinObject.setProteinName("Hypothetical protein");
				proteinList.add(proteinObject);
			}
			proteinIRI = item.getIRI("protein");
			// System.out.println(proteinIRI);
			String interpro = item.getIRI("interpro");
			String idesc = item.getLitString("idesc");
			String sdesc = item.getLitString("sdesc");
			String bdesc = item.getLitString("bdesc");
			Float score = null;
			if (item.getLitString("score") != null) {
				score = Float.parseFloat(item.getLitString("score"));
			}

			if (proteinObject.getProteinName() == "Hypothetical protein") {
				if (score != null && score < 0.00005) {
					// Should always be the case as only blast score is used
					// but placed here if query changes
					if (bdesc != null) {
						// Formatting..
						bdesc = bdesc.replaceAll("(OS=.*|GN=.*|PE=[0-9]+|SV=[0-9]+)", "");
						proteinObject.setProteinName(bdesc);
						proteinObject.setProteinURI(proteinIRI);
						System.out.println("protein name set...");
					}
				}

				ProteinDomain domainX = new ProteinDomain();
				proteinObject.addProteinURI(proteinIRI);

				proteinObject.addDomain(domainX);

				domainX.addDomainURI(interpro);

				// Make the descriptions
				List<String> descriptionList = domainX.getDescriptions();

				if (bdesc != null)
					descriptionList.add(bdesc);
				if (idesc != null)
					descriptionList.add(idesc);
				if (sdesc != null)
					descriptionList.add(sdesc);

				domainX.setDescriptions(descriptionList);

				domainX.setScore(-1);

				domainList.add(domainX);
			}
		}
		for (Protein protein : proteinList) {
			// System.out.println(proteinList.size());

			List<ProteinDomain> domains = protein.getDomains();
			functionCalculator(domains, protein);

			String proteinURI = protein.getProteinURI();
			String product = protein.getProteinName();
			RDFSubject proteinRDF = new RDFSubject(domain, proteinURI, "ssb:Protein");
			proteinRDF.addLit("ssb:product", product.replace("_", " "));
			// System.out.println("URI: " + proteinURI);
			// System.out.println("PRODUCT: " + product);
		}
	}

	private static void functionCalculator(List<ProteinDomain> domains, Protein protein) {
		// Priority naming... based on local knowledge
		String[] no_priority = { "p-loop", "domain-like", "conserved", "binding", "like", "family", "enzyme",
				"containing", "unknown", "uncharacterized", "UPF", "(duf)", "terminal", "helix-turn-helix", "repeat",
				"coil", "domain" };
		String[] low_priority = { "predicted", "homolog", "putative", "related", "associated" };
		String[] higher_priority_spores = { "spore", "spoI", "sporulation", "germination", "capsule" };
		String[] higher_priority_virus = { "virus", "viral", "phage", "transposase", "transposon", "transposition",
				"integrase", "resolvase", "dde superfamily endonuclease" };
		String[] higher_priority_transporter = { "transport", "influx", "efflux", "permease", "intake", "uptake",
				"symport", "antiport", "import", "pump", "exchanger", "channel", "translocase" };
		String[] higher_priority_transcription_factors = { "regul", "repress", "transcription", "zinc-finger",
				"zinc finger" };
		String[] higher_priority_chaperones = { "chaperone", "chaperonin", "heat shock", "heat-shock", "cold-shock",
				"cold shock" };

		// int pscore = -9;
		for (ProteinDomain domain : domains) {
			int annotationScore = protein.getProteinScore();
			List<String> descriptions = domain.getDescriptions();
			// For each description found.. check the best information // Order
			// of description based on coverage?
			for (String description : descriptions) {

				if (description != null) {
					// System.out.println("DESC: " + description);
					int score = 0;

					if (description.equals("")) {
						score = -10;
						// System.out.println("Score killed");
					} else {
						description = description.toLowerCase().replace(",", " ");

						score = matching(description, no_priority, -2, score);
						score = matching(description, low_priority, -1, score);
						score = matching(description, higher_priority_spores, +1, score);
						score = matching(description, higher_priority_virus, +1, score);
						score = matching(description, higher_priority_transporter, +1, score);
						score = matching(description, higher_priority_transcription_factors, +1, score);
						score = matching(description, higher_priority_chaperones, +1, score);
						// Setting the name...
						// System.out.println(
						// "ANNOTSCORE:" + annotationScore + "\tSCORE:" + score
						// + "\tTESTING: " + description);
						if (score > annotationScore) {
							protein.setProteinName(description);
							protein.setProteinScore(score);
						}
					}
				}
			}
		}

	}

	private static int matching(String description, String[] elements, int value, int score) {
		for (String element : elements) {
			if (description.contains(element))
				score += value;
		}
		return score;
	}

	private static void loadRDF() throws Exception {
		String input = arguments.getOptionValue("input");

		System.out.println("HDT file detected");
		String temp = File.createTempFile("turtle", ".tmp").getAbsolutePath();
		HDT hdt = new HDT();
		hdt.hdt2rdf(input, temp);

		System.out.println("TEMP FILE:" + input);
		SAPPSource = new RDFSimpleCon("file://" + temp + "{NT}");
		SAPPSource.setNsPrefix("ssb", "http://csb.wur.nl/genome/");
		SAPPSource.setNsPrefix("protein", "http://csb.wur.nl/genome/protein/");
		SAPPSource.setNsPrefix("biopax", "http://www.biopax.org/release/bp-level3.owl#");
	}
}
