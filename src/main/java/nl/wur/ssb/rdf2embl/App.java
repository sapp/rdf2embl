package nl.wur.ssb.rdf2embl;

import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import nl.wur.ssb.GenBankHandler.data.Record;
import nl.wur.ssb.GenBankHandler.data.RecordBuilder;
import nl.wur.ssb.GenBankHandler.data.ResidueType;
import nl.wur.ssb.GenBankHandler.writer.EmblWriter;
import nl.wur.ssb.GenBankHandler.writer.GenBankWriter;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;

public class App {
	private static RDFSimpleCon SAPPSource;
	private static int codonTable;
	private static int contigSize;
	private static RecordBuilder builder;
	private static CommandLine argument;
	private static String header;
	private static String accession;
	private static String locus;

	public static void main(String[] args) throws Exception {
		// TODO make an option users can choose between gene calling methods, or
		// should it automatically kicks out multiple files per gene calling
		// method?

		Logger.getRootLogger().setLevel(Level.OFF);

		argument = CommandParser.argsValidator(args);

		if (argument.hasOption("pathwaytools")) {
			System.out.println("Source and Gene features are skipped for pathway tools compatability");
		}

		String[] annotation = { "-i", argument.getOptionValue("input") };
		SAPPSource = nl.wur.ssb.proteinAnnotation.App.main(annotation);
		// Check if all genes have locus tags
		for (ResultLine gene : SAPPSource.runQuery("getLocus.txt", false)) {
			throw new Exception("Locus tags are not available for all genes. Please run LocusTagger first");
		}

		for (ResultLine scaffold : SAPPSource.runQuery("getLocus.txt", false)) {
			throw new Exception("Accession numbers are not available for all scaffolds. Please run LocusTagger first");
		}

		buildGenome();
	}

	private static void buildGenome() throws Exception {
		StringWriter buf = new StringWriter();
		GenBankWriter genbankWriter = null;
		EmblWriter emblWriter = null;
		if (argument.getOptionValue("writer").equals("genbank")) {
			genbankWriter = new GenBankWriter(buf);
		} else if (argument.getOptionValue("writer").equals("embl")) {
			emblWriter = new EmblWriter(buf);
		}
		builder = new RecordBuilder();
		// int contigCounter = 1;
		// int locusCount = 1;
		// String locusPrefix =
		// argument.getOptionValue("locus").replaceAll("_$",
		// "");
		// Removal of last _ in case someone passed that in the prefix

		for (ResultLine item : SAPPSource.runQuery("getDnaObjects.txt", false)) {
			String dnaobject = item.getIRI("dna");
			System.out.println(dnaobject);
			builder.locus("LOCUS");

			codonTable = item.getLitInt("table", Integer.parseInt(argument.getOptionValue("codon")));

			String sequence = item.getLitString("sequence");
			contigSize = sequence.length();

			header = item.getLitString("header");
			accession = item.getLitString("accession");
			builder.accession(accession, 1);

			// Now taken care by the LocusTagger module
			// if (argument.hasOption("scaffold") &&
			// !argument.getOptionValue("scaffold").equals("")) {
			// builder.accession(argument.getOptionValue("scaffold") +
			// contigCounter, 1);
			// } else if (header != null) {
			// try {
			// String subheader = header.replace(">", "").split(" ")[0];
			// if (subheader.length() > 15) {
			// // LAST 15 characters of first part of subheader
			// builder.accession(subheader.substring(subheader.length() - 15),
			// 1);
			// builder.nid("ABC");
			// } else {
			// builder.accession(subheader, 1);
			// }
			// } catch (ParseException e) {
			// System.out.println("CATCH " + header);
			// builder.accession("Contig_" + contigCounter, 1);
			// }
			// } else {
			// builder.accession("Contig_" + contigCounter, 1);
			// }
			// contigCounter++;
			Date dt = new Date();
			builder.date(dt, "COMMENT DATE");
			// builder.dblink("SHA384", item.getLitString("sha384"));
			// builder.comment(item.getLitString("sha384"));
			builder.definition(argument.getOptionValue("organism"));
			builder.organism("\"" + argument.getOptionValue("organism") + "\"");
			ArrayList<String> taxonomy = new ArrayList<String>();

			try {
				taxonomy = getLineage(taxonomy, argument.getOptionValue("taxon"));
				taxonomy = invertArray(taxonomy);
				builder.taxonomy(taxonomy);
			} catch (Exception e) {
				builder.taxonomy(null);
			}

			builder.size(contigSize);

			// System.out.println("CIRCULAR " + argument.hasOption("circular"));
			if (argument.hasOption("circular"))
				builder.circular();

			builder.residueType(ResidueType.DNA);

			// builder.organelle("ORGANELLE");
			builder.originName("ORIGIN NAME");
			builder.sequence(sequence);
			// builder.source("GENEX");
			builder.strandType("genomic DNA");
			// builder
			// System.out.println(argument.getOptionValue("projectid"));
			builder.pid(argument.getOptionValue("projectid"));

			builder.taxDivision("PRO");
			builder.data_file_division(argument.getOptionValue("dataclass"));

			builder.db_source("DB_SOURCE");
			// TODO Need to unique it!
			String keys = argument.getOptionValue("dataclass") + "," + argument.getOptionValue("keywords");

			Set<String> keyWordsSet = new LinkedHashSet<>(Arrays.asList(keys.split(",")));
			ArrayList<String> keyWords = new ArrayList<String>();
			for (String key : keyWordsSet) {
				keyWords.add(key);
			}

			builder.keywords(keyWords);
			// REF part

			builder.reference_num(1);
			if (argument.hasOption("title")) {
				String title = argument.getOptionValue("title");
				if (!title.equals("")) {
					builder.title(title);
				}
			}

			if (argument.hasOption("consortium")) {
				String consort = argument.getOptionValue("consortium");
				if (!consort.equals("")) {
					builder.consrtm(consort);
				}
			}

			ArrayList<Integer> start = new ArrayList<Integer>(Arrays.asList(1));
			ArrayList<Integer> end = new ArrayList<Integer>(Arrays.asList(contigSize));
			builder.refereceLocation("REFERENCE_LOCATION", start, end);
			builder.authors(argument.getOptionValue("authors"));
			if (argument.hasOption("journal")) {
				String journal = argument.getOptionValue("journal");
				if (!journal.equals("")) {
					builder.journal(journal);
				}
			}

			builder.startFeatureTable();
			if (!argument.hasOption("pathwaytools")) {
				sourceFeature(builder, item, 0, sequence.length());
			}

			for (ResultLine feature : SAPPSource.runQuery("getFeatures.txt", false, dnaobject))

			{
				locus = feature.getLitString("locus");
				// String type = feature.getIRI("featureType");
				// if (feature.getIRI("featureType").contains("Crispr")) {
				// Skipp crispr for now
				// } else

				if (feature.getIRI("featureType").contains("Assembly_gap")) {
					gapFeature(builder, feature);
				} else {
					if (argument.hasOption("-gapprotein")) {
						sequence = feature.getLitString("sequence");
						int count = StringUtils.countMatches(sequence.toUpperCase(), "N");
						if (count > sequence.length() / 2) {
							System.out.println("Skipping feature: " + feature.getIRI("feature"));
						} else {
							// locus = locusPrefix + "_" + locusCount;
							if (!argument.hasOption("pathwaytools")) {
								geneFeature(builder, feature);
							}
							typeFeature(builder, feature);
							// locusCount++;
						}
					} else {
						// locus = locusPrefix + "_" + locusCount;
						if (!argument.hasOption("pathwaytools")) {
							geneFeature(builder, feature);
						}

						typeFeature(builder, feature);
						// locusCount++;

					}
				}
			}
			builder.endFeatureTable();
			builder.recordEnd();
		}

		for (Record rec : builder.getRecords()) {
			if (argument.getOptionValue("writer").equals("genbank")) {
				genbankWriter.writeRecord(rec);
			} else if (argument.getOptionValue("writer").equals("embl")) {
				emblWriter.writeRecord(rec);
			}

		}
		FileWriter fw = new FileWriter(argument.getOptionValue("output"));
		System.out.println("Saving to: " + new File(argument.getOptionValue("output")).getAbsolutePath());
		fw.write(buf.toString());
		fw.close();
		buf.close();
	}

	private static ArrayList<String> invertArray(ArrayList<String> taxonomy) {
		// invert taxonomy
		ArrayList<String> inverted = new ArrayList<String>();
		for (String element : taxonomy) {
			inverted.add(0, element);
		}
		return inverted;
	}

	private static ArrayList<String> getLineage(ArrayList<String> taxonomy, String taxon) throws Exception {

		String input = "http://sparql.uniprot.org/sparql";
		RDFSimpleCon uniprot = new RDFSimpleCon(input);

		for (ResultLine item : uniprot.runQuery("getLineage.txt", false, taxon)) {
			String parentName = item.getLitString("parentname");
			taxonomy.add(parentName);
			taxon = item.getIRI("parent").toString().replace("http://purl.uniprot.org/taxonomy/", "");
			taxonomy = getLineage(taxonomy, taxon);
		}
		return taxonomy;
	}

	private static void sourceFeature(RecordBuilder builder, ResultLine item, int featureBegin, int featureEnd)
			throws Exception {
		// GENOME FEATURE
		builder.startFeature("source");
		builder.startLocation();
		builder.beginPair();
		builder.exactLoc(featureBegin); // Begin
		builder.exactLoc(featureEnd); // End
		builder.endPair();
		builder.endLocation();
		builder.featureQualifier("organism", "\"" + argument.getOptionValue("organism") + "\"");
		builder.featureQualifier("mol_type", "\"genomic DNA\"");
		builder.featureQualifier("db_xref", "\"taxon:" + argument.getOptionValue("taxon") + "\"");

		if (argument.hasOption("strain") && !argument.getOptionValue("strain").equals("")) {
			builder.featureQualifier("strain", argument.getOptionValue("strain"));
		}

		if (argument.hasOption("sub_strain") && !argument.getOptionValue("sub_strain").equals("")) {
			builder.featureQualifier("sub_strain", argument.getOptionValue("substrain"));
		}

		if (argument.hasOption("note") && !argument.getOptionValue("note").equals("")) {
			String note = argument.getOptionValue("note");
			note = note.replace("$shakey", item.getLitString("sha384"));
			note = note.replace("Xshakey", item.getLitString("sha384"));
			note = note.replace("__cn__", "\n");
			note = note.replace("$header", header);
			note = note.replace("Xheader", header);
			builder.featureQualifier("note", "\"" + note + "\"");
		} else {
			builder.featureQualifier("note",
					"\"Annotation was performed using the Semantic Annotation Platform for Prokaryotes (SAPP) and the sha384 key is "
							+ item.getLitString("sha384") + " and the FASTA header name is: " + header + "\"");
		}
		builder.endFeature();

	}

	private static void gapFeature(RecordBuilder builder, ResultLine feature) throws Exception {

		String[] featureTypeSplit = feature.getIRI("featureType").split("/");
		String featureType = featureTypeSplit[featureTypeSplit.length - 1].toUpperCase();

		builder.startFeature(featureType);
		positioning(feature);

		for (ResultLine gap : SAPPSource.runQuery("getGaps.txt", false, feature.getIRI("feature"))) {

			builder.featureQualifier("estimated_length", String.valueOf(gap.getLitInt("estimated_length")));
			builder.featureQualifier("gap_type", "\"" + String.valueOf(gap.getLitString("gap_type")) + "\"");
			builder.featureQualifier("linkage_evidence", "\"" + gap.getLitString("linkage_evidence") + "\"");
		}
		builder.endFeature();
	}

	private static void typeFeature(RecordBuilder builder, ResultLine feature) throws Exception {

		String[] featureTypeSplit = feature.getIRI("featureType").split("/");
		String featureType = featureTypeSplit[featureTypeSplit.length - 1].toUpperCase();
		// If pathway tools is enabled all features except CDS are ignored
		if (argument.hasOption("-pathwaytools")) {
			if (!featureType.equals("CDS")) {
				return;
			}
		}

		builder.startFeature(featureType);

		positioning(feature);

		builder.featureQualifier("locus_tag", "\"" + locus + "\"");

		if (featureType.equals("CDS")) {
			// double gc = feature.getLitDouble("gc");
			// int partial = feature.getLitInt("partial");

			CDS.featureDomains(SAPPSource, builder, feature.getIRI("feature"), codonTable);
		}
		builder.endFeature();
	}

	private static void positioning(ResultLine feature) throws Exception {
		int featureBegin = feature.getLitInt("begin");
		int featureEnd = feature.getLitInt("end");
		int strand = 1;
		if (featureEnd < featureBegin) {
			strand = -1;
			featureEnd = featureEnd - 1;
		} else if (featureEnd > featureBegin) {
			featureBegin = featureBegin - 1;
		}

		if (strand == 1)
			builder.startLocation();
		else
			builder.startComplement();

		builder.beginPair();

		int partial = 0;

		try {
			partial = feature.getLitInt("partial");
		} catch (Exception e) {
			// No partial information available thus remains 0;
		}

		if (partial == 0) { // (00)
			if (strand == 1) {
				builder.exactLoc(featureBegin); // Begin
				builder.exactLoc(featureEnd); // End
			} else if (strand == -1) {
				builder.exactLoc(featureEnd); // End
				builder.exactLoc(featureBegin); // Begin
				// if partial (0)1, 10, 01
			}
		}
		// if (featureEnd == 2)
		// System.out.println(partial + "\t" + featureBegin + "\t" + featureEnd
		// + "\t" + strand);
		if (partial == 1) { // 01
			if (strand == 1) {
				builder.exactLoc(featureBegin);
				builder.afterPosition(featureEnd);
			} else if (strand == -1) {
				builder.exactLoc(featureEnd);
				builder.afterPosition(featureBegin);
			}
		} else if (partial == 10) {
			if (strand == 1) {
				builder.beforePosition(featureBegin);
				builder.exactLoc(featureEnd);
			} else if (strand == -1) {
				builder.beforePosition(featureEnd);
				builder.exactLoc(featureBegin);
			}
		} else if (partial == 11) {
			if (strand == 1) {
				builder.beforePosition(featureBegin);
				builder.afterPosition(featureEnd);
			} else if (strand == -1) {
				builder.beforePosition(featureEnd);
				builder.afterPosition(featureBegin);
			}

		}

		builder.endPair();
		if (strand == 1) {
			builder.endLocation();
		} else {
			builder.endComplement();
		}
	}

	private static void geneFeature(RecordBuilder builder, ResultLine feature) throws Exception {
		// GENE FEATURE
		// String[] featureTypeSplit = feature.getIRI("featureType").split("/");
		builder.startFeature("gene");
		positioning(feature);
		builder.featureQualifier("locus_tag", "\"" + locus + "\"");
		builder.endFeature();

	}

	// private static void loadRDF() throws Exception {
	// String format = argument.getOptionValue("format");
	// String input = "file://" + argument.getOptionValue("input") + "{" +
	// format + "}";
	// SAPPSource = new RDFSimpleCon(input);
	//
	// }
}
