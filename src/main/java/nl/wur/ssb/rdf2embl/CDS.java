package nl.wur.ssb.rdf2embl;

import java.util.HashSet;
import java.util.Set;

import nl.wur.ssb.GenBankHandler.data.RecordBuilder;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;

public class CDS {

  public static void featureDomains(RDFSimpleCon SAPPSource, RecordBuilder builder,
      String featureURI, int codonTable) throws Exception {
    Set<String> tool = new HashSet<String>();
    builder.featureQualifier("transl_table", String.valueOf(codonTable));

    for (ResultLine domain : SAPPSource.runQuery("getDomainFeatures.txt", false, featureURI)) {
      String db = domain.getLitString("db").replace("interpro", "InterPro");
      String id = domain.getLitString("id");
      builder.featureQualifier("db_xref", "\"" + db + ":" + id + "\"");
      tool.add(domain.getLitString("tool") + " " + domain.getLitString("version"));
    }

    // String comment = "nnotation was achieved with";
    // for (String t : tool)
    // comment = comment + " " + t;
    //
    // builder.featureQualifier("comment",comment);

    for (ResultLine sequence : SAPPSource.runQuery("getProteinSequence.txt", false, featureURI)) {
      builder.featureQualifier("translation", "\"" + sequence.getLitString("sequence") + "\"");
      builder.featureQualifier("product", "\"" + sequence.getLitString("product") + "\"");
    }

  }
}
