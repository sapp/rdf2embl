package nl.wur.ssb.rdf2embl;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class CommandParser {

  public static CommandLine argsValidator(String[] args) throws Exception {
    Options options = new Options();

    // input
    Option option = Option.builder("i").argName("file").desc("RDF input").hasArg(true)
        .required(true).longOpt("input").build();
    options.addOption(option);
    // output
    option = Option.builder("o").argName("file").hasArg(true).desc("Output filename").required(true)
        .longOpt("output").build();
    options.addOption(option);
    // Codon table
    option = Option.builder("c").longOpt("codon").argName("<codon>").hasArg(true)
        .desc("Codon table used").required(false).build();
    options.addOption(option);
    // Writer format
    option = Option.builder("w").longOpt("writer").argName("format").hasArg(true)
        .desc("embl/genbank format").required(true).build();
    options.addOption(option);
    // Genome name
    option = Option.builder("organism").argName("name").hasArg(true).desc("Name of organism")
        .required(true).build();
    options.addOption(option);
    // Gap proteins allowed or not?
    option = Option.builder("gapprotein").argName("name").hasArg(false)
        .desc("Skipping proteins >50% N?").required(false).build();
    options.addOption(option);
    // strain name
    option = Option.builder("strain").argName("name").hasArg(true).desc("Name of strain")
        .required(true).build();
    options.addOption(option);

    // strain name
    option = Option.builder("substrain").argName("name").hasArg(true).desc("Name of sub-strain")
        .required(false).build();
    options.addOption(option);

    // PathwayTools compatability
    option = Option.builder("pathwaytools").argName("ptools").hasArg(false)
        .desc("Pathway tools compatability modues").required(false).build();
    options.addOption(option);

    // paper title
    option = Option.builder("title").argName("title").hasArg(true).desc("Title of publication")
        .required(false).build();
    options.addOption(option);

    // paper journal
    option = Option.builder("journal").argName("journal").hasArg(true).desc("Journal publication")
        .required(false).build();
    options.addOption(option);

    // paper consortium
    option = Option.builder("consortium").argName("consortium").hasArg(true).desc("consortium")
        .required(false).build();
    options.addOption(option);

    // Genome dataclass
    option = Option.builder("dataclass").argName("class").hasArg(true).desc("dataclass")
        .required(true).build();
    options.addOption(option);

    // Circular topology
    option =
        Option.builder("circular").hasArg(false).desc("Circular topology?").required(false).build();
    options.addOption(option);

    // Project id
    option = Option.builder("projectid").argName("id").hasArg(true).desc("ENA Project ID")
        .required(false).build();
    options.addOption(option);

    // locus prefix
    option = Option.builder("locus").argName("name").hasArg(true).desc("Locus prefix")
        .required(true).build();
    options.addOption(option);

    // note information
    option = Option.builder("note").argName("note text").hasArg(true)
        .desc("Note text for annotation").required(false).build();
    options.addOption(option);

    // scaffold prefix
    option = Option.builder("scaffold").argName("scaffold prefix").hasArg(true)
        .desc("Prefix for scaffolds").required(false).build();
    options.addOption(option);

    // keywords
    option = Option.builder("k").argName("keywords").hasArg(true).desc("key,words,separated,by,comma")
        .longOpt("keywords").required(true).build();
    options.addOption(option);

    // authors
    option = Option.builder("a").argName("names").hasArg(true)
        .desc("Author list between '' e.g. 'John Doe, Jane Doe'").longOpt("authors").required(true)
        .build();
    options.addOption(option);

    // taxon
    option = Option.builder("t").argName("taxonid").hasArg(true).desc("Taxon number")
        .longOpt("taxon").required(true).build();
    options.addOption(option);

    CommandLineParser parser = new DefaultParser();
    try {
      CommandLine cmd = parser.parse(options, args);
      return cmd;
    } catch (Exception e) {
      System.out.println(e);
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("Missing arguments, possible options see below", options);
    }
    System.exit(0);
    return null;

  }

}
